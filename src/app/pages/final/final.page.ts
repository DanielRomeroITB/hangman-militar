import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-final',
  templateUrl: './final.page.html',
  styleUrls: ['./final.page.scss'],
})
export class FinalPage implements OnInit {
  winner: string = "";
  vidas: any;
  dificultad: string = "";
  verbo: string = "";
  titulo: string = "";

  constructor(private router: Router, private route: ActivatedRoute, private dataService: DataService) { 
    this.route.queryParams.subscribe(params => {
        if (params && params.dificultad){
          this.dificultad = params.dificultad;
        }

        if (params && params.winner){
          this.winner = params.winner;
        }

        if (params && params.vidas){
          this.vidas = params.vidas;
        }
    });
  }
  inicioPagina() {
    let navigationExtras: NavigationExtras;
    this.router.navigate(['inicio'], navigationExtras);
  }

  juegoPagina() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        dificultad: this.dificultad
      }
    };
    this.router.navigate(['juego'], navigationExtras);
  }

  ionViewWillEnter() {
    this.ngOnInit();
  }

  ngOnInit() {
    if (this.winner == "true"){
      this.titulo = "Felicidades!";
      this.verbo = "adivinado"
    } else if (this.winner == "false") {
      this.titulo = "Mala suerte!";
      this.verbo = "adivinado erroneamente"
    }
  }

}
