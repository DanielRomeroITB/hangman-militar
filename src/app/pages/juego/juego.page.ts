import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-juego',
  templateUrl: './juego.page.html',
  styleUrls: ['./juego.page.scss'],
})
export class JuegoPage implements OnInit {

  dificultad:string = "";
  intentos:string = "";
  palabra:string = "";
  substituida:string = "";
  arrayPalabra: string[];
  winner: boolean = false;
  vidas: any = 0;
  hangman:string = "";
  num = 0;

  constructor(private router: Router, private route: ActivatedRoute, private dataService: DataService) { 
    this.route.queryParams.subscribe(params => {
        if (params && params.dificultad){
          this.dificultad = params.dificultad;
        }
    });
  }
  
  ionViewWillEnter() {
    this.enableButtons();
    this.ngOnInit();
  }

  finalPagina() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        winner: this.winner,
        vidas: this.vidas,
        dificultad: this.dificultad
      }
    };
    this.router.navigate(['final'], navigationExtras);
  }

  substituir(letra, event) {
    event.srcElement.setAttribute("disabled", true);
      for (let i = 0; i < this.palabra.length; i++) {
        if(letra == this.palabra.charAt(i)) {
          this.arrayPalabra[i] = this.arrayPalabra[i].replace("_", letra);
          this.substituida = this.arrayPalabra.join(" ");
          if (!this.substituida.includes("_")){
            this.winner = true;
            this.finalPagina();
          }
        }
      }

      if (!this.palabra.includes(letra)) {
        this.num += 1;
        this.hangman = "../../../assets/hangman" + this.num + ".png";
        this.vidas -= 1;
        this.intentos = "Te quedan " + this.vidas + " intentos"
        if (this.vidas == 0) {
          this.winner = false;
          this.finalPagina();
        }
      }
  }

  enableButtons() {
    const alphabet = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","ñ","o","p","q","r","s","t","u","v","w","x","y","z"];

    alphabet.forEach(letter => {
      document.getElementById(letter).setAttribute("disabled", "false");
    });
  }

  ngOnInit() {
    this.vidas = 6;
    this.num = 1;
    this.hangman = "../../../assets/hangman1.png";

    this.palabra = this.dataService.getDifficulty(this.dificultad);
    this.intentos = "Te quedan " + this.vidas + " intentos";

    this.substituida = this.palabra;

    for (let i = 0; i < this.palabra.length; i++) {
      var letra = this.palabra.charAt(i);
      this.substituida = this.substituida.replace(letra, "_ ");
    }
    this.arrayPalabra = this.substituida.split(" ");
  }

}
