import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  dificultad:string = "";
  toast: Promise<void>;

  constructor(private router: Router, private toastcontroller: ToastController) {}

  juegoPagina() {
    let navigationExtras: NavigationExtras = {
      queryParams:{
        dificultad: this.dificultad
      }
    };
    this.router.navigate(['juego'], navigationExtras);
  }

  showToast() {
    this.toast = this.toastcontroller.create({
      message: 'Modo Facil: Palabras mas sencillas o cortas \nModo Dificil: Palabras mas complejas o largas \nTienes 6 intentos para acertar la palabra.',
      duration: 2000
    }).then((toastData) => {
      console.log(toastData);
      toastData.present();
    });
  }

  ngOnInit() {
  }

}
