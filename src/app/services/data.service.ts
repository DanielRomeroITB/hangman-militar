import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() {}

   facil = ["rifle", "pistola", "granada", "casco", "tanque"];
   dificil = ["kevlar", "submarino", "coronel", "mortero", "apache"];

   getDifficulty(dificultad) {
    if (dificultad == 'facil'){
      return this.facil[Math.floor(Math.random() * this.facil.length)]
    } else {
      return this.dificil[Math.floor(Math.random() * this.dificil.length)]
    }
   }

}
